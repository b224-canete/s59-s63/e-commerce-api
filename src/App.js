import { useState, useEffect } from 'react';
import { Container } from 'react-bootstrap';
import { BrowserRouter as Router } from 'react-router-dom';
import { Routes, Route } from 'react-router-dom';
import AppNavbar from './components/AppNavbar';

import Cart from './pages/Cart';
import CartProductView from './pages/CartProductView';
import Dashboard from './pages/Dashboard';
import DashboardAdd from './pages/DashboardAdd';
import DashboardUpd from './pages/DashboardUpd';
import Error from './pages/Error';
import Home from './pages/Home';
import Login from './pages/Login';
import Logout  from './pages/Logout';
import Products  from './pages/Products';
import ProductView  from './pages/ProductView';
import Register from './pages/Register';
import UserProfile from './pages/UserProfile';
import ViewOrders from './pages/ViewOrders';
import './App.css';
import { UserProvider } from './UserContext';




function App() {

  const [user, setUser] = useState({
    id: null,
    isAdmin: null
});


const unsetUser = () => {
 localStorage.clear()
}

useEffect(() => {
    fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
     headers: {
         Authorization: `Bearer ${localStorage.getItem('token')}`
     }
    })
    .then(res => res.json())
    .then(data => {
     
     if (typeof data._id !== "undefined") {

         setUser({
             id: data._id,
             fullName: data.fullName,
             email:data.email,
             mobileNo: data.mobileNo,
             deliveryAdd: data.deliveryAdd,
             isAdmin: data.isAdmin
         })
     } else { 
         setUser({
             id: null,
             fullName: null,
             mobileNo: null,
             deliveryAdd: null,
             isAdmin: null
         })
     }
    })
}, [])



return (

    <UserProvider value={{user, setUser, unsetUser}}>
        <Router>
            <AppNavbar />
            <Container className="appContainer">
                <Routes>
                    <Route path="/" element={<Home/>} />
                    <Route path="/cart" element={<Cart/>} />
                    <Route path="/products/cartview/:productId" element={<CartProductView/>} />
                    <Route path="/dashboard" element={<Dashboard/>} />
                    <Route path="/dashboardAdd" element={<DashboardAdd/>} />
                    <Route path="/products/:productId" element={<DashboardUpd/>} />
                    <Route path="/login" element={<Login/>} />
                    <Route path="/logout" element={<Logout/>} />
                    <Route path="/products" element={<Products/>} />
                    <Route path="/products/view/:productId" element={<ProductView/>} />
                    <Route path="/register" element={<Register/>} />
                    <Route path="/profile" element={<UserProfile/>} />
                    <Route path="/viewOrders" element={<ViewOrders/>} />
                    <Route path='/*' element={<Error/>} />
                </Routes>
            </Container>
        </Router>
    </UserProvider>


  );
}

export default App;
