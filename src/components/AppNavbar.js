//Old practice in importing components
// import Navbar from 'react-bootstrap/Navbar';
// import Nav from 'react-bootstrap/Nav';


// Better practice in importing components
import { useContext } from 'react';
import { Navbar, Nav, Container } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';



export default function AppNavbar() {


	const { user } = useContext(UserContext)


	return (

		<Navbar className="px-5 navbar" expand="md" sticky="top">
		  <Container>
		    <Navbar.Brand as={Link} to="/" className="logo">VaseShop<i className="fa-solid fa-store"></i></Navbar.Brand>
		    <Navbar.Toggle aria-controls="basic-navbar-nav" />
		    <Navbar.Collapse id="basic-navbar-nav" className="justify-content-end">
		      <Nav >
		        <Nav.Link as={Link} to="/" className="nav-items">Home</Nav.Link>

		        {
		        	(user.isAdmin) ?
			        	<Nav.Link as={Link} to="/dashboard" className="nav-items">Dashboard</Nav.Link>
			        	:
			        	<Nav.Link as={Link} to="/products" className="nav-items">Products</Nav.Link>
		        }

		        

		        {
		        	(user.id !== null) ?
		        		<>
		        		<Nav.Link as={Link} to="/logout" className="nav-items">Logout</Nav.Link>
                		<Nav.Link as={Link} to="/profile" className="cart"><i className="fa-solid fa-user"></i></Nav.Link>
		        		</>
		        		:
		        		<>
		        		<Nav.Link as={Link} to="/login" className="nav-items">Login</Nav.Link>
		        		<Nav.Link as={Link} to="/register" className="nav-items">Register</Nav.Link>
                		<Nav.Link as={Link} to="/login" className="cart"><i className="fa-solid fa-user"></i></Nav.Link>

		        		</>
		        }

                {
                	(user.isAdmin === false) ?
                		<Nav.Link as={Link} to="/cart" className="cart"><i className="fa-solid fa-cart-shopping"></i></Nav.Link>
                		:
                		<>
                		</>
                }	

                {
                	(user.id === null) ?
                		<Nav.Link as={Link} to="/login" className="cart"><i className="fa-solid fa-cart-shopping"></i></Nav.Link>
                		:
                		<>
                		</>
                }		        


		      </Nav>
		    </Navbar.Collapse>
		  </Container>
		</Navbar>

	)
}