import { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function ProfileCard({itemsProp}) {

	return (

		<tr id="allCard-tr">
		    <td className="allCard-td allCard-img"><img src={itemsProp.orderDetails[0].variation} alt="image" className="img-fluid"/></td>
		    <td className="allCard-td">{itemsProp.orderDetails[0].productName}</td>
		    <td className="allCard-td">{itemsProp.orderDetails[0].size}</td>
		    <td className="allCard-td">{itemsProp.orderDetails[0].orderQty}</td>
		    <td className="allCard-td">{itemsProp.orderDetails[0].price}</td>
		    <td className="allCard-td">{itemsProp.totalAmount}</td>

		   
		</tr>
	)
}
