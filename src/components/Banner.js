import { Row, Col, Card } from 'react-bootstrap';


export default function Banner({bannerProp}) {


	const { id, photo, content, subcontent } = bannerProp;

	return (

		// <Row>
		// 	<Col>
		// 		<Card className="bannerCard">
		// 		  <Card.Body>
		// 			 <Card.Img id="banner-image" src={photo} alt="icon"/>
		// 		  	 <Card.Title id="banner-content">{content}</Card.Title>   
		// 		     <Card.Subtitle id="banner-subcontent">{subcontent}</Card.Subtitle>
		// 		  </Card.Body>
		// 		</Card>
		// 	</Col>
		// </Row>

		<div className="banner-sub">
			<img src={photo} alt="icon"/>
			<div className="banner-info">
				<h3>{content}</h3>
				<span>{subcontent}</span>
			</div>
		</div>

	)
}

