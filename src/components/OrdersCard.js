import { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function Orders({ordersProp}) {
	
	const { deliveryAdd, modeOfPayment, orderId, orderQty, purchasedOn, totalAmount, userFullName, variation } = ordersProp; 
// 
	return (

		<tr id="allCard-tr">
		    <td className="allCard-td allCard-img"><img src={variation} alt="image" className="img-fluid"/></td>
		    <td className="allCard-td all-td-desc" >{orderId}</td>
		    <td className="allCard-td">{userFullName}</td>
		    <td className="allCard-td">{orderQty}</td>
		    <td className="allCard-td">{totalAmount}</td>
		    <td className="allCard-td">{deliveryAdd}</td>
		    <td className="allCard-td">{modeOfPayment}</td>
		    <td className="allCard-td">{purchasedOn.substring(0, 10)} {purchasedOn.substring(11, 16)}</td>

		</tr>
	)
}

