import { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useParams, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function CartCard({itemsProp}) {

	let item = itemsProp.productId;

	return (

		<tr id="allCard-tr">
		    <td className="allCard-td allCard-img"><img src={itemsProp.variation} alt="image" className="img-fluid"/></td>
		    <td className="allCard-td">{itemsProp.productName}</td>
		    <td className="allCard-td">{itemsProp.orderQty}</td>
		    <td className="allCard-td">{itemsProp.price}</td>
		    <td className="allCard-td">{itemsProp.subtotal}</td>
		    <td className="allCard-td">
		    	<div id="allCard-div-btn">
		    		<Button className="reg-btn allCard-btn" variant="success" type="submit" id="submitBtn" as={Link} to={`/products/cartview/${itemsProp.productId}`}>
		    		  Buy
		    		</Button>
		    	</div>
		    </td>
		   
		</tr>
	)
}

