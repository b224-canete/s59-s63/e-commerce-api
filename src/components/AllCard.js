import { useState, useEffect } from 'react';
import { Button } from 'react-bootstrap';
import { useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';

export default function AllCard({productProp}) {
	
	const navigate = useNavigate();

	const [name, setName] = useState(productProp.name);
	const [description, setDescription] = useState(productProp.description);
	const [isActive, setIsActive] = useState(productProp.isActive);
	const [qtyOnHand, setQtyOnHand] = useState(productProp.qtyOnHand);
	// const [count, setCount] = useState(0);
	const [size, setSize] = useState(productProp.size);
	const [variation, setVariation] = useState(productProp.variation);
	const [price, setPrice] = useState(productProp.price);
	const [productId, setProductId] = useState(productProp._id);

	let orderArr = productProp.userOrder;

	let count = orderArr.reduce(function(prev, current) {
	  return prev + +current.orderQty
	}, 0);

	const archive = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/archive`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {

			if(data === false) {

				let timerInterval;
				Swal.fire({
				  html: 'Failed to disable the Product',
				  timer: 800,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})

				navigate("/dashboard")

			} else {

				let timerInterval
				Swal.fire({
				  html: 'Product disabled',
				  timer: 800,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})

				navigate("/dashboard")
			}
		})
	}

	const activate = (productId) => {

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}/activate`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
		})
		.then(res => res.json())
		.then(data => {

			if(data === false) {

				let timerInterval;
				Swal.fire({
				  html: 'Failed to activate the Product.',
				  timer: 800,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})

				navigate("/dashboard")

			} else {

				let timerInterval;
				Swal.fire({
				  html: 'Product activated.',
				  timer: 800,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})

				navigate("/dashboard")
			}
		})
	}

	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/view/${productId}`)
		.then(res => res.json())
		.then(data => {

			setName(data.name);
			setDescription(data.description);
			setIsActive(data.isActive);
			setQtyOnHand(data.qtyOnHand);
			setSize(data.size);
			setVariation(data.variation);
			setPrice(data.price);
		})
	});

	return (

		<tr id="allCard-tr">
		    <td className="allCard-td allCard-img"><img src={variation} alt="image" className="img-fluid"/></td>
		    <td className="allCard-td">{name}</td>
		    <td className="allCard-td all-td-desc">{description}</td>
		    <td className="allCard-td">{size}</td>
		    <td className="allCard-td">{qtyOnHand}</td>
		    <td className="allCard-td">{count}</td>
		    <td className="allCard-td">{price}</td>

		    {
		    (isActive) ?
		    <>
			    <td className="allCard-td">Available</td>
			    <td className="allCard-td">
			    	<div id="allCard-div-btn">
			    		<Button className="reg-btn allCard-btn" variant="primary" type="submit" id="submitBtn" as={Link} to={`/products/${productId}`}>
			    		  Update
			    		</Button>
			    		<Button className="reg-btn allCard-btn" variant="danger" type="submit" id="submitBtn" onClick={() => archive(productId)}>
			    		  Disable
			    		</Button>
			    	</div>
			    </td>
		    </>
		    :
		    <>
			    <td className="allCard-td">Not Available</td>
			    <td className="allCard-td">
			    	<div id="allCard-div-btn">
			    		<Button className="reg-btn allCard-btn" variant="primary" type="submit" id="submitBtn" as={Link} to={`/products/${productId}`}>
			    		  Update
			    		</Button>
			    		<Button className="reg-btn allCard-btn" variant="success" type="submit" id="submitBtn" onClick={() => activate(productId)}>
			    		  Enable
			    		</Button>
			    	</div>
			    </td>
		    </>
			}
		</tr>
	)
}

