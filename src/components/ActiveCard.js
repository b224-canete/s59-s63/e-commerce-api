import { useState, useContext } from 'react';
import { Nav } from 'react-bootstrap';
import { useParams, Navigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ActiveCard({productProp}) {
	
	const { user } = useContext(UserContext)

	const [name, setName] = useState(productProp.name);
	const [description, setDescription] = useState(productProp.description);
	const [isActive, setIsActive] = useState(productProp.isActive);
	const [qtyOnHand, setQtyOnHand] = useState(productProp.qtyOnHand);
	const [size, setSize] = useState(productProp.size);
	const [variation, setVariation] = useState(productProp.variation);
	const [price, setPrice] = useState(productProp.price);
	const [productId, setProductId] = useState(productProp._id);

	let orderArr = productProp.userOrder;

	let count = orderArr.reduce(function(prev, current) {
	  return prev + +current.orderQty
	}, 0);


	function addCart() {

		fetch(`${process.env.REACT_APP_API_URL}/users/cart/add`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				orderQty: 1
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === false) {

				let timerInterval;
				Swal.fire({
				  html: 'Oops! Something Went Wrong. Please Try Again.',
				  timer: 1500,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})

			} else {

				let timerInterval;
				Swal.fire({
				  html: 'Added to Cart',
				  timer: 1500,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})
			}
		})
	}



	return (

		<div className="active">
		    <span className="sold">{count} <span id="sold-txt">sold</span></span>
		    <div className="image">
		        <img src={variation} alt="Item Image"/>
		        <div className="icons">
		            <Nav.Link className="shop-btn" as={Link} to={`/products/view/${productId}`}>Details</Nav.Link>

		            {
		            (user.isAdmin === false) ?
		            <Nav.Link className="shop-btn cart-btn" onClick={(addCart)}>Add cart</Nav.Link>
		            :
		            <Nav.Link className="shop-btn cart-btn" as={Link} to={`/login`}>Add cart</Nav.Link>
			        }
		        </div>
		    </div>
		    <div className="content">
		        <h3>{name}</h3>
		        <div className="price"> &#8369; {price} </div>
		    </div>
		</div>

	)
}