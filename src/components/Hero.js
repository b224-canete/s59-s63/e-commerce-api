import { useContext } from 'react';
import { Button } from 'react-bootstrap';
import { Link } from 'react-router-dom';
import UserContext from '../UserContext';


export default function Hero() {

	const { user } = useContext(UserContext)


	return (

		<div className="main">
			<div className="content">
				<h1>Aesthetic Vases</h1>
				<span> Decorative and Beautiful Vases </span>
				<p>Let's start brightening up your home  to make your day! We have varieties of vases for you to choose from. Find one that makes you can't take your eyes off.</p>
				
				{
					(user.isAdmin) ?
					<Button as={Link} to="/dashboard" id="btn-hero">Admin dashboard</Button>
					:
					<Button as={Link} to="/products" id="btn-hero">Shop Now</Button>
				}
			</div>
		</div>

	)
}
