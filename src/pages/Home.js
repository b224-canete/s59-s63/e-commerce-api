import Banner from '../components/Banner';
import Hero from '../components/Hero';
import bannerData from '../data/bannerData';

export default function Home() {

	const content = bannerData.map(banner => {
		return (
			<Banner key={banner.id} bannerProp={banner} />
		)
	})

	return (

		<>
			<Hero />
			<div className="banner-div" >
				{content}
			</div>
		</>

	)
}