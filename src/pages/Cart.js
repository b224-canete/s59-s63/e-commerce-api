import { useState, useEffect, useContext } from 'react';
import { Button, Row, Col, Container, Table } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import CartCard from '../components/CartCard';	

export default function Cart() {

	const { user, setUser } = useContext(UserContext);

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setProducts(data.inCart.map(item => {
				return (
					<CartCard key={item._id} itemsProp={item} />
				)
			}))
		})
	}, [])


	return (

		(user.isAdmin !== false) ?
			<Navigate to="/" /> 
		:
			<div id="db-main">
				<div className="headers">Shopping Cart</div>

				<div className="db-table-div">
				<Table responsive striped hover id="db-table">
				      <thead id="table-head">
				        <tr>
				            <th className="table-head-items">Image</th>
				            <th className="table-head-items">Product</th>
				            <th className="table-head-items">Qty</th>
				            <th className="table-head-items">Price</th>
				            <th className="table-head-items">Subtotal</th>
				            <th className="table-head-items">Check Out</th>
				        </tr>
				      </thead>
				      <tbody>

				      	{products}

				      </tbody>
				</Table>
				</div>

			</div>
	)
}