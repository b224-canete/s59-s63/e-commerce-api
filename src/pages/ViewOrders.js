import { useState, useEffect, useContext } from 'react';
import { Button, Row, Col, Container, Table } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import OrdersCard from '../components/OrdersCard';	

export default function ViewOrders() {

	const { user, setUser } = useContext(UserContext);

	const [orders, setOrders] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/orders`, {
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {	

			
			setOrders(data.map(order => {
				return (
					<OrdersCard key={order._id} ordersProp={order} />
				)
			}))
		})
	}, [])



	return (

		(user.isAdmin) ?
		
			<div id="db-main">
				<div className="headers">Admin Dashboard</div>

				<Row id="bd-row" style={{width: '100vw', margin: 0, paddingRight: '5%', paddingLeft: '5%'}}>
					<Col className="db-col">
						<Button as={Link} to={`/dashboardAdd`} className="reg-btn" variant="primary" size="sm" color="success" type="submit" id="submitBtn">
						  Add Product
						</Button> {''}
						<Button as={Link} to={`/dashboard`} className="reg-btn" variant="secondary" size="sm" type="submit" id="submitBtn">
						  View All Products
						</Button> {''} 
						<Button as={Link} to={`/dashboardView`} className="btn reg-btn" variant="success" size="sm" type="submit" id="submitBtn">
						  View All Orders
						</Button>{''}
					</Col>
				</Row>

				<div className="db-table-div">
				<Table responsive striped hover id="db-table">
				      <thead id="table-head">
				        <tr>
				            <th className="table-head-items">Item</th>
				            <th className="table-head-items">Order</th>
				            <th className="table-head-items">Buyer</th>
				            <th className="table-head-items">Order Qty</th>
				            <th className="table-head-items">Total Amount</th>
				            <th className="table-head-items">Delivery Address</th>
				            <th className="table-head-items">Mode of Payment</th>
				            <th className="table-head-items">Date Purchased</th>
				        </tr>
				      </thead>
				      <tbody>

				      	{orders}

				      </tbody>
				</Table>
				</div>

			</div>
		:
		<Navigate to="/products" /> 
	)
}
