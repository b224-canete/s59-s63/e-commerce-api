import { useState, useEffect, useContext } from 'react';
import { Button, Row, Col, Container, Table, Form } from 'react-bootstrap';
import { useParams, useNavigate, Link, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function DashboardUpd() {


	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();


	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [qtyOnHand, setQtyOnHand] = useState(0);
	const [size, setSize] = useState("");
	const [variation, setVariation] = useState("");
	const [price, setPrice] = useState(0);

	const [imageDisplay, setImageDisplay] = useState("../images/image.png");
	const [isActive, setIsActive] = useState(false);



	function updateProduct(e) {

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/products/${productId}`, {
			method: "PUT",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				name: name,
				description: description,
				qtyOnHand: qtyOnHand,
				size: size,
				variation: variation,
				price: price
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === false) {

				let timerInterval;
				Swal.fire({
				  html: 'Something went wrong. Please try again.',
				  timer: 1500,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})


			} else {

				let timerInterval;
				Swal.fire({
				  html: 'Succesfully Updated!',
				  timer: 1500,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})
			}
		})
	}

	useEffect(() => {
		if((
			name !== "" && 
			description !== "" && 
			qtyOnHand !== "" &&
			qtyOnHand !== 'e' &&
			qtyOnHand > 0 && 
			size !== "" &&
			variation !== "" &&
			variation.substring(0, 8) === "https://" &&
			price !== "" &&
			price !== "e" &&
			price > 0)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [name, description, qtyOnHand, size, variation, price]);



	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/view/${productId}`)
		.then(res => res.json())
		.then(data => {

		setName(data.name);
		setDescription(data.description);
		setQtyOnHand(data.qtyOnHand);
		setSize(data.size);
		setVariation(data.variation);
		setPrice(data.price)

		})

	}, [productId])


	useEffect(() => {
		if((
			
			variation !== "" &&
			variation.substring(0, 8) === "https://")) {
			setImageDisplay(variation)
		} else {
			setImageDisplay("../images/image.png")
		}
	}, [variation]);





	return (

		(user.isAdmin) ?

			<div id="db-main">
				<div className="headers">Admin Dashboard</div>

				<Row id="bd-row" style={{width: '100vw', margin: 0, paddingRight: '5%', paddingLeft: '5%'}}>
					<Col className="db-col">
						<Button as={Link} to={`/dashboardAdd`} className="reg-btn" variant="primary" size="sm" color="success" type="submit" id="submitBtn">
						  Add Product
						</Button> {''}
						<Button as={Link} to={`/dashboard`} className="reg-btn" variant="secondary" size="sm" type="submit" id="submitBtn">
						  View All Products
						</Button> {''} 
						<Button as={Link} to={`/viewOrders`} className="btn reg-btn" variant="success" size="sm" type="submit" id="submitBtn">
						  View All Orders
						</Button>{''}
					</Col>
				</Row>

				<div className="db-panel-div">
					<Row>
						<Col xs={4} id="panel-img-col">
							<div className="db-pre-img">
								<img src={imageDisplay} alt="Image" className="img-fluid"/>
							</div>
						</Col>
					
						<Col xs={12} md={8}>
							<Form className="mb-3 mnt-0 mx-2">

						      <Row className="mb-3">
						      	<Col className="mt-3 px-0" xs={12} md={6}>
							        <Form.Group as={Col} controlId="formGridEmail">
							          <Form.Label>Name</Form.Label>
							          <Form.Control 
							          	  size="sm"
								          type="text" 
								          placeholder="Product Name" 
								          value={name}
								          onChange={ e => setName(e.target.value)}
								          required
							          />
							        </Form.Group>
							    </Col>
							    <Col className="mt-3 px-0" xs={12} md={6}>
							        <Form.Group as={Col} controlId="formGridPassword">
							          <Form.Label>Product Image Link</Form.Label>
							          <Form.Control 
							          	  size="sm"
								          type="text" 
								          placeholder="https://" 
								          value={variation}
								          onChange={ e => setVariation(e.target.value)}
								          required
							          />
							        </Form.Group>
							    </Col>
						      </Row>

						      <Form.Group className="mb-3" controlId="formGridAddress1">
						        <Form.Label>Description</Form.Label>
						        <Form.Control 
						        	  size="sm"
							          type="text" 
							          placeholder="Product Details" 
							          value={description}
							          onChange={ e => setDescription(e.target.value)}
							          required
						          />
						      </Form.Group>

						      <Row className="mb-3">
						        <Form.Group as={Col} controlId="formGridCity">
						          <Form.Label>Qty</Form.Label>
						          <Form.Control 
						           	  size="sm"
							          type="number" 
							          placeholder="pcs" 
							          value={qtyOnHand}
							          onChange={ e => setQtyOnHand(e.target.value)}
							          required
						          />
						        </Form.Group>

						        <Form.Group as={Col} controlId="formGridCity">
						          <Form.Label>Size</Form.Label>
						          <Form.Control 
						          	  size="sm"
							          type="text" 
							          value={size}
							          onChange={ e => setSize(e.target.value)}
							          required
						          />
						        </Form.Group>

						        <Form.Group as={Col} controlId="formGridZip">
						          <Form.Label>Price</Form.Label>
			                      <Form.Control 
			                    	  size="sm"
			          	              type="number" 
			          	          	  placeholder="Php" 
			          	          	  value={price}
			          	          	  onChange={ e => setPrice(e.target.value)}
			          	          	  required
			                    />
						        </Form.Group>
						      </Row>

						      <Row>
						      	<Col xs={6} style={{ display: "flex", justifyContent: "flex-end" }}>
				      			      <Button className="reg-btn" variant="danger" type="submit" id="submitBtn" as={Link} to={"/dashboard"}>
				      	    		  Go Back
				      	    		</Button>
				      	    	</Col>
				      	    	<Col xs={6}>
				      	    		{
				      	    			isActive ?
				      	    			<Button className="reg-btn" variant="primary" type="submit" id="submitBtn" onClick={(e) => updateProduct(e)}>
				      	    			  Update
				      	    			</Button>
				      	    			:
				      	    			<Button className="reg-btn" variant="primary" type="submit" id="submitBtn" disabled>
				      	    			  Update
				      	    			</Button>
				      	    		}
				      	    	</Col>
						      </Row>

						    </Form>
						</Col>	
					</Row >		
				</div>

			</div>
			:
			<Navigate to="/products" />
	)
}