import { useState, useEffect, useContext } from 'react';
import { Col, Row, Form, Button, Card } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function Register() {

	const { user } = useContext(UserContext);

	const navigate = useNavigate();

	const [fullName, setFullName] = useState('');
	const [email, setEmail] = useState('');
	const [password1, setPassword1] = useState('');
	const [password2, setPassword2] = useState('');
	const [mobileNo, setMobileNo] = useState('');
	const [deliveryAdd, setDeliveryAdd] = useState('');

	const [isActive, setIsActive] = useState(false);

	

	function registerUser(e) {
		e.preventDefault()


		fetch(`${process.env.REACT_APP_API_URL}/users/register`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				fullName: fullName,
				email: email,
				password: password1,
				mobileNo: mobileNo,
				deliveryAdd: deliveryAdd
			})
		})
		.then(res => res.json())
		.then(data => {

			{
				if(data.msg === "Email Exist") {
					Swal.fire({
						title: "Duplicate Email Found",
						icon: "error",
						text: "Please provide a different email."
					})
				} else if(data === true) {

					setFullName("");
					setEmail("");
					setPassword1("");
					setPassword2("");
					setMobileNo("");
					setDeliveryAdd("");

					Swal.fire({
						title: "Registration Successful",
						icon: "success",
						text: "Happy to have you VaseShopper!"
					})

					navigate("/login")

				} else {
					Swal.fire({
						title: "Opps! something went wrong",
						icon: "error",
						text: "Please try again."
					})

				}
			}
		})
	}

	useEffect(() => {
		if((
			fullName !== "" && 
			email !== "" && 
			password1 !== "" && 
			password2 !== "" &&
			mobileNo.length >= 11 && 
			mobileNo !== "" &&
			deliveryAdd !== "") && 
			(password1 === password2)) {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [fullName, email, password1, password2, mobileNo, deliveryAdd]);


	return (

		(user.id !== null) ?
			<Navigate to="/" />
			:
			<div id="reg-log">
				<div id="reg-log-div">
					<div className="img-div"><img src="../images/reg-log.webp" alt="VaseShop" className="img-fluid"/></div>
					<div className="form-div">
						<h5 className="text-center my-3">Sign <span id="shop">Up</span></h5>
						<Form className="reg-form" onSubmit={e => registerUser(e)}>

							<Row className="mb-2" >
							<Form.Label className="form-label" column="sm" lg={5}>Full Name :</Form.Label>
							<Col>
							<Form.Control 
							  	size="sm"
							  	type="text" 
							  	placeholder="Enter full name"
							  	value={fullName}
							  	onChange={e => setFullName(e.target.value)}
							  	required
							  />
							</Col>
							</Row>

							<Row className="mb-2" >
							<Form.Label column="sm" lg={5}>Email address :</Form.Label>
							<Col>
							<Form.Control 
								size="sm"
								type="email" 
								placeholder="Enter email"
								value={email}
								onChange={e => setEmail(e.target.value)}
								required
							/>
							</Col>
							</Row>

							<Row className="mb-2" >
							<Form.Label column="sm" lg={5}>Password :</Form.Label>
							<Col>
							<Form.Control 
								size="sm"
								type="password" 
								placeholder="Password"
								value={password1}
								onChange={e => setPassword1(e.target.value)}
								required 
							/>
							</Col>
							</Row>

							<Row className="mb-2" >
							<Form.Label column="sm" lg={5}>Verify Password :</Form.Label>
							<Col>
							<Form.Control 
								size="sm"
								type="password" 
								placeholder="Verify Password"
								value={password2}
								onChange={e => setPassword2(e.target.value)}
								required 
							/>
							</Col>
							</Row>

							<Row className="mb-2" >
							<Form.Label column="sm" lg={5}>Mobile No :</Form.Label>
							<Col>
							<Form.Control 
							  	size="sm"
							  	type="text" 
							  	placeholder="09999999999"
							  	value={mobileNo}
							  	onChange={e => setMobileNo(e.target.value)}
							  	required
							  />
							  </Col>
							</Row>

							<Row className="mb-2" >
							<Form.Label column="sm" lg={5}>Delivery Address :</Form.Label>
							<Col>
							<Form.Control 
							  	size="sm"
							  	type="text" 
							  	placeholder="Enter delivery Address"
							  	value={deliveryAdd}
							  	onChange={e => setDeliveryAdd(e.target.value)}
							  	required
							  />
							  </Col>
							</Row>

							<div style={{ display: "flex", justifyContent: "flex-end" }}>
								{
									isActive ?
										<Button className="reg-btn" variant="primary" type="submit" id="submitBtn">
										  Submit
										</Button>
										:
										<Button className="reg-btn" variant="primary" type="submit" id="submitBtn" disabled>
										  Submit
										</Button>

								}
							</div>
						</Form>
						<p>Already have an account? <Card.Link as={Link} to="/login">Click here </Card.Link> to log in</p>
					</div>
				</div>
			</div>

	)
}