import { useState, useEffect, useContext } from 'react';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import ActiveCard from '../components/ActiveCard';	

export default function Products() {

	const { user, setUser } = useContext(UserContext);

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/products`, {
		})
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map(product => {
				return (
					<ActiveCard key={product._id} productProp={product} />
				)
			}))
		})
	}, [])



	return (

		(user.isAdmin) ?
		<Navigate to="/dashboard" /> 
		:
		<div className="products" id="products">
			<h1 id="store-heading"> Enjoy <span> VaseShopping</span> </h1>

			<div className="box-container">

			{products}
			    
			</div>
		</div>

	)
}