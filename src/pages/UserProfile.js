import { useState, useEffect, useContext } from 'react';
import { Button, Row, Col, Container, Table } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import ProfileCard from '../components/ProfileCard';	

export default function UserProfile() {

	const { user, setUser } = useContext(UserContext);

	const [history, setHistory] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setHistory(data.orderedProduct.map(item => {
				return (
					<ProfileCard key={item._id} itemsProp={item} />
				)
			}))
		})
	}, [])

	return (

		(user.isAdmin) ?
		<div id="db-main">
			<div className="headers">User Profile</div>

			<div className="px-5 py-3" id="profile-mid">
				<h1>Warm Greetings Vase<span id="shop">Shop</span> Admin!</h1>
				<h3>Your details :</h3>
				<Row> 
					<Col xs={6} md={3}>
						<h6 className="profile-item">Name :</h6>
						<h6 className="profile-item">Email :</h6>
						<h6 className="profile-item">Contact No :</h6>
						<h6 className="profile-item">Delivery Address :</h6>
					</Col>
					<Col  xs={6} md={9}>
						<h6 className="profile-item">{user.fullName}</h6>
						<h6 className="profile-item">{user.email}</h6>
						<h6 className="profile-item">{user.mobileNo}</h6>
						<h6 className="profile-item">{user.deliveryAdd}</h6>
					</Col>
				</Row>
			</div>
			<div id="for-layout">
			</div>

		</div>

		:
		
		<div id="db-main">
			<div className="headers">User Profile</div>

			<div className="px-5 py-3" id="profile-mid">
				<h1>Hello Vase<span id="shop">Shop</span>per!</h1>
				<h3>Your details :</h3>
				<Row> 
					<Col xs={6} md={3}>
						<h6 className="profile-item">Name :</h6>
						<h6 className="profile-item">Email :</h6>
						<h6 className="profile-item">Contact No :</h6>
						<h6 className="profile-item">Delivery Address :</h6>
					</Col>
					<Col  xs={6} md={9}>
						<h6 className="profile-item">{user.fullName}</h6>
						<h6 className="profile-item">{user.email}</h6>
						<h6 className="profile-item">{user.mobileNo}</h6>
						<h6 className="profile-item">{user.deliveryAdd}</h6>
					</Col>
				</Row>
			</div>

			<div className=" py-3 mb-0" id="profile-mid-2">
				<h2 id="prof-mid2-txt" className="mb-0">Orders Completed</h2>
			</div>


			<div className="db-table-div mt-0" id="db-table-div-profile">
			<Table responsive striped hover id="db-table">
			      <thead id="table-head">
			        <tr>
			            <th className="table-head-items">Image</th>
			            <th className="table-head-items">Product</th>
			            <th className="table-head-items">Size</th>
			            <th className="table-head-items">Ordered</th>
			            <th className="table-head-items">Price</th>
			            <th className="table-head-items">Total</th>
			        </tr>
			      </thead>
			      <tbody>

			      	{history}

			      </tbody>
			</Table>
			</div>

		</div>
	)
}