import { useState, useEffect, useContext } from 'react';
import { Row, Col, Form, Button, Card } from 'react-bootstrap';
import { Navigate, useNavigate, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function Login() {


	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const [email, setEmail] = useState("");
	const [password, setPassword] = useState("");
	
	const [isActive, setIsActive] = useState("");




	function authenticate(e) {
	
		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/login`, {
			method: 'POST',
			headers: {
				'Content-Type': 'application/json'
			},
			body: JSON.stringify({
				email: email,
				password: password
			})
		})
		.then(res => res.json())
		.then(data => {
			

			if(typeof data.access !== "undefined") {
				localStorage.setItem('token', data.access)
				retrieveUserDetails(data.access)

				Swal.fire({
					title: "Login Successful",
					icon: "success",
					text: "Welcome back VaseShopper!"
				})

				navigate("/")
				
			}  else {
				Swal.fire({
					title: "Authentication Failed",
					icon: "error",
					text: "Please check your login details and try again."
				})
			}
		})

		setEmail("");
		setPassword("");

	}


	const retrieveUserDetails = (token) => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
				Authorization: `Bearer ${token}`
			}
		})
		.then(res => res.json())
		.then(data => {

			setUser({
				id: data._id,
				fullName: data.fullName,
				email:data.email,
				mobileNo: data.mobileNo,
				deliveryAdd: data.deliveryAdd,
				isAdmin: data.isAdmin
			})
		})
	}


	useEffect(() => {
		if(email !== "" && password !== "") {
			setIsActive(true)
		} else {
			setIsActive(false)
		}
	}, [email, password]);

	return (
		
		(user.id !== null) ?

			<Navigate to="/" />

			:
			<div id="reg-log">
				<div id="reg-log-div">
					<div className="img-div"><img src="../images/reg-log.webp" alt="VaseShop" className="img-fluid"/></div>
					<div className="form-div">
						<h3 className="welcome"> Start Vase<span id="shop-2">Shop</span>ping! </h3>
						<h5 className="text-center my-3">Log In</h5>
						<Form className="reg-form" onSubmit={e => authenticate(e)}>

							<Row className="mb-2" >
							<Form.Label column="sm" lg={5}>Email address :</Form.Label>
							<Col>
							<Form.Control 
								size="sm"
								type="email" 
								placeholder="Enter email"
								value={email}
								onChange={e => setEmail(e.target.value)}
								required
							/>
							</Col>
							</Row>

							<Row className="mb-2" >
							<Form.Label column="sm" lg={5}>Password :</Form.Label>
							<Col>
							<Form.Control 
								size="sm"
								type="password" 
								placeholder="Password"
								value={password}
								onChange={e => setPassword(e.target.value)}
								required 
							/>
							</Col>
							</Row>


							<div style={{ display: "flex", justifyContent: "flex-end" }}>
								{
									isActive ?
										<Button className="reg-btn" variant="primary" type="submit" id="submitBtn">
										  Submit
										</Button>
										:
										<Button className="reg-btn" variant="primary" type="submit" id="submitBtn" disabled>
										  Submit
										</Button>

								}
							</div>
						</Form>
						<p id="login-text">Don't have an account yet? <Card.Link as={Link} to="/register">Click here </Card.Link> to register.</p>
					</div>
				</div>
			</div>

			)
}