import { useState, useEffect, useContext } from 'react';
import { Button, Row, Col, Container, Table } from 'react-bootstrap';
import { Link, Navigate } from 'react-router-dom';
import UserContext from '../UserContext';

import AllCard from '../components/AllCard';	

export default function Dashboard() {

	const { user, setUser } = useContext(UserContext);

	const [products, setProducts] = useState([]);

	useEffect(() => {
		fetch(`${process.env.REACT_APP_API_URL}/products/allProducts`, {
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {
			
			setProducts(data.map(product => {
				return (
					<AllCard key={product._id} productProp={product} />
				)
			}))
		})
	}, [])



	return (

		(user.isAdmin) ?
		
			<div id="db-main">
				<div className="headers">Admin Dashboard</div>

				<Row id="bd-row" style={{width: '100vw', margin: 0, paddingRight: '5%', paddingLeft: '5%'}}>
					<Col className="db-col">
						<Button as={Link} to={`/dashboardAdd`} className="reg-btn" variant="primary" size="sm" color="success" type="submit" id="submitBtn">
						  Add Product
						</Button> {''}
						<Button as={Link} to={`/dashboard`} className="reg-btn" variant="secondary" size="sm" type="submit" id="submitBtn">
						  View All Products
						</Button> {''} 
						<Button as={Link} to={`/viewOrders`} className="btn reg-btn" variant="success" size="sm" type="submit" id="submitBtn">
						  View All Orders
						</Button>{''}
					</Col>
				</Row>

				<div className="db-table-div">
				<Table responsive striped hover id="db-table">
				      <thead id="table-head">
				        <tr>
				            <th className="table-head-items">Image</th>
				            <th className="table-head-items">Product</th>
				            <th className="table-head-items">Description</th>
				            <th className="table-head-items">Size</th>
				            <th className="table-head-items">Inventory</th>
				            <th className="table-head-items">Units Sold</th>
				            <th className="table-head-items">Price</th>
				            <th className="table-head-items">Availability</th>
				            <th className="table-head-items">Actions ~</th>
				        </tr>
				      </thead>
				      <tbody>

				      	{products}

				      </tbody>
				</Table>
				</div>

			</div>
		:
		<Navigate to="/products" /> 
	)
}