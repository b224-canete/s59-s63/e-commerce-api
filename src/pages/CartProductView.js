import { useState, useEffect, useContext } from 'react';
import { Container, Card, Button, Row, Col, Form, Modal } from 'react-bootstrap';
import{ useParams, useNavigate, Link, Navigate } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';


export default function CartProductView() {

	const { user, setUser } = useContext(UserContext);

	const navigate = useNavigate();

	const { productId } = useParams();

	const [name, setName] = useState("");
	const [description, setDescription] = useState("");
	const [qtyOnHand, setQtyOnHand] = useState(0);
	const [size, setSize] = useState("");
	const [variation, setVariation] = useState("");
	const [price, setPrice] = useState(0);
	const [count, setCount] = useState(0);
	const [orderQty, setOrderQty] = useState(1);
	const [totalAmount, setTotalAmount] = useState(0);
	const [modeOfPayment, setModeOfPayment] = useState("Cash on Delivery");
	const [userOrderArray, setUserOrderArray] = useState([]);
	const [orderId, setOrderId] = useState("");

	const [modalShow, setModalShow] = useState(false);


	function minus() {

		if(orderQty === 1) {
			setOrderQty(1)
			setTotalAmount(orderQty*price);
		} else {
			setOrderQty(orderQty - 1)
			setTotalAmount(orderQty*price);
		}
	};

	function plus() {

		if(orderQty >= qtyOnHand) {
			setOrderQty(orderQty)
			setTotalAmount(orderQty*price);
		} else {
			let num = parseInt(orderQty, 10)
			setOrderQty(num + 1)
			setTotalAmount(orderQty*price);
		}
	};

	function enterQty(e) {

		e.preventDefault();

		if(orderQty > qtyOnHand) {
			setOrderQty(qtyOnHand)
			setTotalAmount(orderQty*price);
		} else if(orderQty < 1) {
			setOrderQty(1)
			setTotalAmount(orderQty*price);
		} else {
			setOrderQty(orderQty)
			setTotalAmount(orderQty*price);
		}
	};

	function verify() {

		if(user.isAdmin === false ) {
			setModalShow(true)
		} else {
			navigate("/login")
		}
	}

	const options = [
	  {value: 'Cash on Delivery', text: 'Cash on Delivery'},
	  {value: 'Shop now, Pay Later', text: 'Shop now, Pay Later'},
	  {value: 'Payment Center', text: 'Payment Center'},
	  {value: 'Credit / Debit Card', text: 'Credit / Debit Card'},
	  {value: 'Online Banking', text: 'Online Banking'},
	];

	function modeChange(e) {

	  e.preventDefault();

	  setModeOfPayment(e.target.value);
	};

	function MydModalWithGrid(props) {

	  return (
	    <Modal {...props} aria-labelledby="contained-modal-title-vcenter">
	      <Modal.Header closeButton id="modal-header">
	        <Modal.Title id="contained-modal-title-vcenter">
	          Checkout
	        </Modal.Title>
	      </Modal.Header>
	      <Modal.Body className="show-grid px-0 pb-0">
	        <Container>
	          <Row className="modal-row">
	            <Col xs={12} md={12}>
	              <h6>Delivery Address</h6>
	              <p className="modal-user">{user.fullName} | {user.mobileNo}</p>
	              <p className="modal-user">{user.deliveryAdd}</p>
	            </Col>
	          </Row>

	          <Row className="modal-row pt-2">
	            <Col xs={12} md={6}>
	              <h6><i className="fa-solid fa-dollar-sign" id="dollar"></i> Payment Option</h6>
	            </Col>
	            <Col xs={12} md={6}>

	            	<select value={modeOfPayment} onChange={e => modeChange(e)}> 
	            	  {options.map(option => (
	            	    <option key={option.value} value={option.value}>
	            	      {option.text}
	            	    </option>
	            	  ))}
	            	</select>

	            </Col>
	          </Row>

	          <Row className="modal-row pt-2">
	            <Col xs={6} md={6}>
	              <h6>Details</h6>
	              <p className="modal-user">Item</p>
	              <p className="modal-user">Qty order</p>
	              <p className="modal-user">Price</p>
	              <p className="modal-user total-payment">Total Payment</p>
	            </Col>
	            <Col xs={6} md={6} style={{
                        textAlign:"right"
                      }} className="pr-3">
	              <h6> -------------</h6>
	              <p className="modal-user">{name}</p>
	              <p className="modal-user">{orderQty}</p>
	              <p className="modal-user">{price}</p>
	              <p className="modal-user total-payment">Php {totalAmount}</p>
	            </Col>
	          </Row>


	        </Container>
	      </Modal.Body>
	      <Modal.Footer>
	        <Button variant="danger" onClick={props.onHide}>Cancel</Button>
	        <Button variant="primary" onClick={(e) => orderProduct(e)}>Proceed</Button>
	      </Modal.Footer>
	    </Modal>
	  );
	}


	function orderProduct(e) {

		e.preventDefault()

		fetch(`${process.env.REACT_APP_API_URL}/users/order`, {
			method: "POST",
			headers: {
				'Content-Type': 'application/json',
				Authorization: `Bearer ${localStorage.getItem('token')}`
			},
			body: JSON.stringify({
				productId: productId,
				orderQty: orderQty,
				modeOfPayment: modeOfPayment
			})
		})
		.then(res => res.json())
		.then(data => {

			if(data === true) {

				setModalShow(false);

				let timerInterval;
				Swal.fire({
				  html: 'Thank You for VaseShopping With Us!',
				  timer: 2000,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }

				})


			} else {

			let timerInterval;
				Swal.fire({
				  html: 'Oops! Something Went Wrong. Please Try Again.',
				  timer: 2000,
				  timerProgressBar: true,
				  didOpen: () => {
				    Swal.showLoading()
				    const b = Swal.getHtmlContainer().querySelector('b')
				    timerInterval = setInterval(() => {
				    }, 100)
				  },
				  willClose: () => {
				    clearInterval(timerInterval)
				  }
				})
			}
		})
	}


	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/products/view/${productId}`)
		.then(res => res.json())
		.then(data => {

		objFunc(data);

		setName(data.name);
		setDescription(data.description);
		setQtyOnHand(data.qtyOnHand);
		setSize(data.size);
		setVariation(data.variation);
		setPrice(data.price);
		setTotalAmount(data.price);
		})

		function objFunc(data) {

			let countsum = data.userOrder.reduce(function(prev, current) {
					  return prev + +current.orderQty
					},0);
			setCount(countsum)
		}

	}, [productId, modalShow])




	useEffect(() => {

		fetch(`${process.env.REACT_APP_API_URL}/users/details`, {
			headers: {
			    Authorization: `Bearer ${localStorage.getItem('token')}`
			}
		})
		.then(res => res.json())
		.then(data => {

		userFunc(data);

		})

		function userFunc(data) {

			const x = data.inCart.findIndex(item => {
				return item.productId === productId;
				 })
			let holderCart = data.inCart[x]

			setOrderQty(holderCart.orderQty)
		}

	}, [productId])

	return (

		(user.isAdmin !== false ) ?
		<Navigate to="/" /> 
		:

		<Container id="product-view"> 
			<Row id="product-view-row">
				<Col className="product-view-col" xs={12} md={6}>
					<div className ="product-view-div">
						<img src={variation} alt="" />
					</div>
				</Col >
					
				<Col className="product-view-col" xs={12} md={6}>
					<div className="py-4 pl-2" id="details">
						<div className="product-details-div mb-3">
							<div >
								<h3>{name}</h3>
							</div>
						</div>
						<div className="product-details-div mb-4 pt-1 pl-2" id="price-head">
							<div id="view-price">
								<h4> &#8369; {price}</h4>
							</div>
							<div>
								<p>{count} sold</p>
							</div>
						</div>
						<div id="details-body">

							<Row className="mb-3">
								<Col sm={4} className="mb-1">
									Description:
								</Col>
								<Col sm={8}>
									{description}
								</Col>
							</Row>

							<Row className = "mb-4">
								<Col sm={4} className="mb-1">
									Size:
								</Col>
								<Col sm={8}>
									{size}
								</Col>
							</Row>

							<Row className = "mb-4">
								<Col sm={4} className="mb-1">
									Qty:
								</Col>
								<Col>
									<Row>
										<Col xs={1} sm={1} className="p-1 m-0 plus-minus-btn">
										<i className="fa-regular fa-square-minus plus-minus" onClick={(minus)}></i>
										</Col>
										<Col xs={2} sm={2} id="qty-col" className="p-0 m-0">
											
											<Form className="reg-form" onSubmit={e => enterQty(e)}>
											<Form.Control
											  	size="sm"
											  	type="number" 
											  	value={orderQty}
											  	onChange={e => setOrderQty(e.target.value)}
											  	required
											  />
											 </Form>
											 
										</Col>
										<Col xs={1} sm={1} className="p-1 m-0 plus-minus-btn">
										<i className="fa-regular fa-square-plus plus-minus" onClick={(plus)}></i>
										</Col>
										<Col xs={6} sm={7} className="p-1 m-0" id="available-col">
											<p id="available-units">{qtyOnHand} pieces available</p>
										</Col>

									</Row>
								</Col>
							</Row>

						    <Row>
						      	<Col xs={6} style={{ display: "flex", justifyContent: "flex-end" }}>
				      			      <Button className="reg-btn" variant="danger" type="submit" id="submitBtn" as={Link} to="/cart">
				      	    		  Cancel
				      	    		</Button>
				      	    	</Col>
				      	    	<Col xs={6}>
				      	    		  <Button className="reg-btn" variant="success" type="submit" id="submitBtn" onClick={(verify)}>
				      	    			  Buy Now
				      	    		  </Button>

				      	    		  <MydModalWithGrid show={modalShow} onHide={() => setModalShow(false)}/>
				      	    	</Col>
						      </Row>

						</div>
					</div>
				</Col>	
			</Row>
		</Container>
	)
}