const bannerData = [

	{
		id: "1",
		photo: "images/icon-1.png",
		content: "Free Shipping",
		subcontent: "On All Orders",
	},
	{
		id: "2",
		photo: "../images/icon-2.png",
		content: "COD",
		subcontent: "Payment Option",
	},
	{
		id: "3",
		photo: "../images/icon-3.png",
		content: "Discounts",
		subcontent: "On Selected Items",
	},
	{
		id: "4",
		photo: "../images/icon-4.png",
		content: "Payday Sale!",
		subcontent: "Enjoy Shopping.",
	}	

]

export default bannerData;